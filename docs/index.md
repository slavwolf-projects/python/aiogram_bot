```{include} ../README.md
---
end-before: <!-- github-only -->
---
```

[license]: license
[command-line reference]: usage

```{toctree}
---
hidden:
maxdepth: 1
---

usage
reference
License <license>
Changelog <https://github.com/slavwolf/aiogram_bot/releases>
```
