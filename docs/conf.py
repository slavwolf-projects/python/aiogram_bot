"""Sphinx configuration."""
project = "AIOGram Bot"
author = "SlavWolf"
copyright = "2022, SlavWolf"
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_click",
    "myst_parser",
]
autodoc_typehints = "description"
html_theme = "furo"
