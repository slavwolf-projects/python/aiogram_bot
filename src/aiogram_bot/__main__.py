"""Command-line interface."""
import click


@click.command()
@click.version_option()
def main() -> None:
    """The AIOGram Bot."""


if __name__ == "__main__":
    main(prog_name="aiogram_bot")  # pragma: no cover
