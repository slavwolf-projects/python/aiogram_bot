# AIOGram Bot

[![PyPI](https://img.shields.io/pypi/v/aiogram_bot.svg)][pypi_]
[![Status](https://img.shields.io/pypi/status/aiogram_bot.svg)][status]
[![Python Version](https://img.shields.io/pypi/pyversions/aiogram_bot)][python version]
[![License](https://img.shields.io/pypi/l/aiogram_bot)][license]

[![Read the documentation at https://aiogram_bot.readthedocs.io/](https://img.shields.io/readthedocs/aiogram_bot/latest.svg?label=Read%20the%20Docs)][read the docs]
[![Tests](https://github.com/slavwolf/aiogram_bot/workflows/Tests/badge.svg)][tests]
[![Codecov](https://codecov.io/gh/slavwolf/aiogram_bot/branch/main/graph/badge.svg)][codecov]

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)][pre-commit]
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)][black]

[pypi_]: https://pypi.org/project/aiogram_bot/
[status]: https://pypi.org/project/aiogram_bot/
[python version]: https://pypi.org/project/aiogram_bot
[read the docs]: https://aiogram_bot.readthedocs.io/
[tests]: https://github.com/slavwolf/aiogram_bot/actions?workflow=Tests
[codecov]: https://app.codecov.io/gh/slavwolf/aiogram_bot
[pre-commit]: https://github.com/pre-commit/pre-commit
[black]: https://github.com/psf/black

## Features

- TODO

## Requirements

- TODO

## Installation

You can install _AIOGram Bot_ via [pip] from [PyPI]:

```console
$ pip install aiogram_bot
```

## Usage

Please see the [Command-line Reference] for details.

## License

Distributed under the terms of the [WTFPL][license],
_AIOGram Bot_ is free and open source software.

## Issues

If you encounter any problems,
please [file an issue] along with a detailed description.

## Credits

This project was generated from [@cjolowicz]'s [Hypermodern Python Cookiecutter] template.

[@cjolowicz]: https://github.com/cjolowicz
[pypi]: https://pypi.org/
[hypermodern python cookiecutter]: https://github.com/cjolowicz/cookiecutter-hypermodern-python
[file an issue]: https://github.com/slavwolf/aiogram_bot/issues
[pip]: https://pip.pypa.io/
[wtfpl]: http://www.wtfpl.net/

<!-- github-only -->

[license]: https://gitlab.com/slavwolf-projects/python/aiogram_bot/-/blob/master/LICENSE
[contributor guide]: https://github.com/slavwolf/aiogram_bot/blob/main/CONTRIBUTING.md
[command-line reference]: https://aiogram_bot.readthedocs.io/en/latest/usage.html
